package com.favor.ct.app;

import android.app.Application;
import android.widget.Toast;

import com.onesignal.OneSignal;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class MainApplication extends Application {
    private static final String ONESIGNAL_APP_ID = "df67a989-2247-4b3f-9fa4-0d892c0027f1";

    @Override
    public void onCreate() {
        super.onCreate();

        yandexMetric();
        startOneSignal();
    }

    void yandexMetric() {
        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("e4d6df1b-0e06-4eea-8cfb-6c106dc2574a").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);
    }

    void startOneSignal() {
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }
}
